function getBeef(){
    return 'Ambil daging burger';
}
function cookBeef(beef){
    console.log(beef)
    return 'Panggang daging burger';
}
function getBuns(){
    return 'Siapkan roti burger';
}
function putBeefBetweenBuns(patty, buns){
    console.log(patty);
    console.log(buns);
    console.log('Letakkan daging diantara roti burger');
    return 'Burger siap disantap';
}
const makeBurger = () => {
    const beef = getBeef();
    const patty = cookBeef(beef);
    const buns = getBuns();
    const burger = putBeefBetweenBuns(patty, buns);
    return burger;
  };
  
const burger = makeBurger();
console.log(burger);