var hello = function() {
    console.log('Hello there');
}
function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
async function sleep(fn, ...args) {
    console.log('wait a second...')
    await timeout(1000);
    return fn(...args);
}
sleep(hello);
console.log('Welcome to Javascript World');