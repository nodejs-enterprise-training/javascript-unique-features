function myDisplayer(some) {
    console.log(some);
  }
  
  function myFirst() { //Defined earlier
    myDisplayer("Hello");
  }
  
  function mySecond() {
    myDisplayer("Goodbye");
  }

  mySecond(); //Called first
  myFirst();
  