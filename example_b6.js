function getBeef(cb){
    console.log('Ambil daging burger');
    cb();
}
function cookBeef(cb){
    console.log('Panggang daging burger');
    cb();
}
function getBuns(cb){
    console.log('Siapkan roti burger');
    cb();
}
function putBeefBetweenBuns(){
    console.log('Letakkan daging diantara roti burger');
}

getBeef(function(){
    cookBeef(function(){
        getBuns(putBeefBetweenBuns);
    })
});