function myDisplayer(some) { //Defined earlier
    console.log("The result is",some);
  }
  
function myCalculator(num1, num2, myCallback) {
    let sum = num1 + num2;
    myCallback(sum);
  }
  
myCalculator(5, 5, myDisplayer); //Caller first